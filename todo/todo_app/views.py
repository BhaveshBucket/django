from django.shortcuts import render
from django.http import HttpResponseRedirect
from todo_app.models import TodoItem


def todoView(request):
    all_todo_items = TodoItem.objects.all()
    return render(request, 'todo.html', {'all_items':all_todo_items})

def addTodo(request):
    new_item = TodoItem(content=request.POST['item'])
    if request.POST['item'] is not None:
        new_item.save()
    return HttpResponseRedirect('/todo/')
    # c = request.POST['content']
    # new_item = TodoItem(content=c)

def deleteTodo(request, todo_id):
    item_to_delete = TodoItem.objects.get(id=todo_id)
    item_to_delete.delete()
    return HttpResponseRedirect('/todo/')